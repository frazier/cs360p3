package com.ssu.finaltext;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.ssu.finaltext.game.Event;
import com.ssu.finaltext.game.FinalText;
import com.ssu.finaltext.game.Option;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by sean on 12/12/13.
 */
public class FinalTextGame {


    ArrayList<FinalText> gameObjects;
    String jsonFileName;
    JSONArray jArray;
    Context context;


    //ArrayList<String> options;
    ArrayList<String> status;
    ArrayList<String> event;
    ArrayList<String> option;
    ArrayList<String> response;
    ArrayList<String> Response;

    private Event currentEvent;

    public FinalTextGame(String jsonFileName){
        gameObjects = new ArrayList<FinalText>();
        currentEvent = null;
        this.jsonFileName = jsonFileName;
        init();
        System.out.println("In FinalTextGame Constructor");
    }

    public boolean play () {
        return true;
    }

    public void init(){
        // Sets the very first object in the arraylist to a final text game - this is for saving
        //FinalText game = new FinalText("Game1", FinalText.Type.GAME,"Final Text Game");
        //gameObjects.add(game);

        initGameObjects();
        System.out.println("In Init");
    }

    public void initGameObjects(){

        if (this.jsonFileName.equals("")){System.err.println("Error, json file not specified!");return;}
        loadJsonData(this.jsonFileName);
        System.out.println("In Init GameObjects");

        // load up all the json data and create objects based on whatever types they are.

    }

    public Event optionSelected(String id){
        boolean flag = true;
        int index = 0;
        while (flag){
            if (gameObjects.get(index).hasID(id)){
                return (Event)gameObjects.get(index);
            }
            index ++;
        }
        return new Event();
    }

    public Event getCurrentEvent () {
        return currentEvent;
    }

    public void setCurrentEvent () {
    }

    public ArrayList<Option> getEventOptions () {
        ArrayList<String> optionsIDs = currentEvent.getOptions();
        ArrayList<Option> optionsArray = new ArrayList<Option>();
        System.out.println("in getEventOptions");
        boolean flag = true;
        int index = 0;
        while (flag == true && index != gameObjects.size()){
            if(optionsIDs.contains(gameObjects.get(index).getId())){
                optionsArray.add((Option)gameObjects.get(index));
            }
            if (optionsArray.size() > 3){
                flag = false;
            }
            index ++;
        }
        return optionsArray;
    }


    public Event getNextEvent(int position){
        String optionID = currentEvent.getOptionSelected(position);
        Option tmpoption = null;
        ArrayList<Event> eventsArray = new ArrayList<Event>();
        System.out.println("in getNextEvent");
        boolean flag = true;
        int index = 0;

        while (flag == true && index < gameObjects.size()){
            if(optionID.equals(gameObjects.get(index).getId())){
                tmpoption = (Option)gameObjects.get(index);
                flag = false;
            }
            index ++;
        }
        System.out.println("Checking if option is null... ");
        if (tmpoption == null) return null;
        index = 0;
        flag = true;
        String tmpEventID = tmpoption.getRandomEventID();
        System.out.println("Option was not null, attempting to generate random event! The new event's id is: " + tmpEventID + " searching gameObjects...");
        while(flag && index < gameObjects.size()){
            System.out.println("made it to random event while loop game object id: " + gameObjects.get(index).getId());

            if (tmpEventID.equals(gameObjects.get(index).getId())){
                currentEvent = (Event) gameObjects.get(index);
                System.out.println("Generated a random event: " + currentEvent.getText() + "!");
                flag = false;
            }
            index++;
        }
        return currentEvent;
    }

    public void loadJsonData(String jsonFileName){

        event = new ArrayList<String>();

        option = new ArrayList<String>();

        response = new ArrayList<String>();
        Response = new ArrayList<String>();

        AsyncHttpClient client = new AsyncHttpClient();
        // Spin up a new thread, and fetch the JSON
        //for (int i = 0; i < json.length; i++) {
        client.get(jsonFileName, new AsyncHttpResponseHandler() {

            // When the JSON has been fetched, we will process it
            // The JSON is simply a string at this point. Now because the JSON is formated as a
            // JSON Array, we will parse it as an Array, and then loop over each JSON Object
            // Fetch that object, and parse out two values from it, and put them into our
            // Simple Object Class.
            @Override
            public void onSuccess(String response) {
                try {
                    jArray = new JSONArray(response); // Parse JSON String to JSON Array
                    System.out.println(jArray.length() + "This is the JArray Length");
                    for (int i = 0; i < jArray.length(); ++i) { // Loop over Array


                        JSONObject jObject = jArray.getJSONObject(i); // Fetch the ith JSON Object
                        // from the JSON Array
                        if (jObject.getString(("Type")).equals("event") || jObject.getString(("Type")).equals("response")) {
                            System.out.println("In JSON event");
                            Event gameEvent = new Event();
                            gameEvent.setText(jObject.getString("String"));
                            gameEvent.setGType(FinalText.Type.EVENT);
                            gameEvent.setID(jObject.getString("objectId"));
                            gameEvent.setEventOptions(jObject.getString("optionId"));
                            gameEvent.setEventCondition(jObject.getInt("Condition"));
                            if (jObject.getBoolean("inBattle"))
                                gameEvent.setStartsBattle(true);
                            else
                                gameEvent.setStartsBattle(false);
                            gameObjects.add(gameEvent);
                        }
                        else if (jObject.getString(("Type")).equals("option")) {
                            System.out.println("In JSON option");
                            Option gameOption = new Option();
                            gameOption.setText(jObject.getString("String"));
                            gameOption.setGType(FinalText.Type.OPTION);
                            gameOption.setID(jObject.getString("objectId"));
                            gameOption.setPossibleEvents(jObject.getString("possibleEvents"));
                            gameObjects.add(gameOption);
                        }

                    }
                    System.out.println(gameObjects.size() + "This is the gameObjects Size");
                } catch (JSONException e) {
                    Log.d("JSON Parse", e.toString());
                }
            }
        });
    }

    public Event createRandomEvent(int range){
        Random generator = new Random(System.currentTimeMillis());
        int randomEvent = generator.nextInt(range);
        currentEvent = (Event) this.gameObjects.get(randomEvent);

        return currentEvent;
    }

    public Event startEvent () {
        currentEvent = (Event) this.gameObjects.get(0);
        return currentEvent;

    }


}
