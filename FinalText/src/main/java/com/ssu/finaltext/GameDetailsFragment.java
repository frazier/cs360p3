package com.ssu.finaltext;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

/**
 * Created by sean on 12/3/13.
 */
public class GameDetailsFragment extends Fragment{
    private String item;
    private String event;
    private List<String> events;
    private List<Bitmap> images;
    ArrayAdapter<String> adapter;
    //ArrayAdapter<Bitmap> imageAdapter;
    Context context = getActivity();
    private ImageView itemImageView;
    private TextView itemTextView;
    private ImageView eventImageView;
    private ImageView eventsImageView;
    private TextView eventTextView;
    private ListView eventListView;
    private ProgressBar characterProgress;
    private ProgressBar worldProgress;
    private Bitmap itemBitmap;
    private Bitmap eventBitmap;

    public GameDetailsFragment(List<String> events, List <Bitmap> images) {
        this.events = events;
        this.images = images;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity;
    }

    public void setImageView (Bitmap imageBitmap){
        this.itemBitmap = imageBitmap;
        //this.eventBitmap = imageBitmap;
        itemImageView.setImageBitmap(imageBitmap);
        //eventsImageView.setImageBitmap(imageBitmap);
        //eventImageView.setImageBitmap(imageBitmap);
    }
    public void setEventImageView (Bitmap imageBitmap){
        //this.itemBitmap = imageBitmap;
        this.eventBitmap = imageBitmap;
        //itemImageView.setImageBitmap(imageBitmap);
        //eventsImageView.setImageBitmap(imageBitmap);
        eventImageView.setImageBitmap(imageBitmap);
    }

    public void setProgress (int damage, int attack) {
        characterProgress.setProgress(characterProgress.getProgress()-damage);
        worldProgress.setProgress(worldProgress.getProgress()-attack);
        //characterProgress.refreshDrawableState();
        //worldProgress.refreshDrawableState();
    }

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_details_layout, container, false);

        /*itemTextView = (TextView) view.findViewById(R.id.gameTextView);
        itemTextView.setText(item);

        eventTextView = (TextView) view.findViewById(R.id.secondTextView);
        eventTextView.setText(item);*/

        eventListView = (ListView)view.findViewById(R.id.eventListView);
        adapter = new ArrayAdapter<String>(context, R.layout.event_textview, events);
        eventListView.setAdapter(adapter);
        //eventListView.setOnItemClickListener(null);
        eventListView.setSelection(events.size()-1);

        characterProgress = (ProgressBar)view.findViewById(R.id.characterBar);
        characterProgress.setProgress(109);
        worldProgress = (ProgressBar)view.findViewById(R.id.worldBar);
        worldProgress.setProgress(109);

/*
        imageAdapter = new ArrayAdapter<Bitmap>(context, R.layout.event_list_view_layout, images);
        eventListView.addView(eventsImageView);
*/
        /*(itemTextView = (TextView) view.findViewById(R.id.eventTextView);
        itemTextView.setText(event);*/

        itemImageView = (ImageView) view.findViewById(R.id.gameImageView);
        eventImageView = (ImageView) view.findViewById(R.id.secondImageView);

        if (itemBitmap != null){
            itemImageView.setImageBitmap(itemBitmap);
            eventImageView.setImageBitmap(itemBitmap);
        }
        return view;
    }
}
