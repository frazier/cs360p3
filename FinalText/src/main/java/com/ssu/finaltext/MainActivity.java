package com.ssu.finaltext;

        import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.BinaryHttpResponseHandler;
        import com.ssu.finaltext.game.FinalText;
        import com.ssu.finaltext.game.Option;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends Activity implements GameOptionsFragment.OnDetailView {

    ArrayList<String> options;
    ArrayList<String> status;
    ArrayList<String> event;
    ArrayList<String> option;
    ArrayList<String> response;
    ArrayList<String> Response;
    //ArrayList<FinalText> event;
    static ArrayList<Bitmap> images;
    FinalTextGame game;

    boolean welcome = true;
    boolean isFighter = true;
    boolean changed = false;
    String change = "";

    private Random generator;
    private boolean inCombat = false;

    String tempUrl;
    int prevPos = 0;
    GameOptionsFragment gameOptionsFragment;
    GameDetailsFragment gameDetailsFragment;

    public String json = "https://bitbucket.org/abretow/json_files/raw/49f79e6384bbe3842af684b0a8b6592be853dca3/EventStrings.json";

    static String [] urls = {
            "http://images.wikia.com/streetfighter/images/archive/e/e5/20110413090131!Super_Street_Fighter_II_X_Art_Ryu_1.jpg",
            "http://files.parse.com/9ad5e385-b168-4704-bb2d-761fc31ce3d5/ec3e776d-6c73-4d89-a0e2-00a82d26becb-FinalText.jpg",
            "http://thesalesblog.com/wp-content/uploads/2011/05/shutterstock_75839950.jpg",
            "http://fc07.deviantart.net/fs71/i/2011/028/7/9/monster_face_sketch_by_eemeling-d387yaj.jpg",
            "http://fc07.deviantart.net/fs31/i/2008/213/9/e/Hollow_Log_stock_by_chamberstock.jpg",
            "http://blog.spoongraphics.co.uk/wp-content/uploads/2012/fun-monster-character/vector-monster-sm.jpg"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        game = new FinalTextGame(json);

        options = new ArrayList<String>();




        //for(int i=0; i<4; i++){
        options.add("                            Play!");
        // }

        event = new ArrayList<String>();

        option = new ArrayList<String>();

        response = new ArrayList<String>();
        Response = new ArrayList<String>();

        // Rather than loop, will need to change based on previous event occurrence
        //for(int i=0; i<4; i++){
        event.add("Welcome to FinalText!\n\nYou are now embarking on a Journey through Text!\n\nTo begin the game, press Play! below");
        //}

        images = new ArrayList<Bitmap>();

        for (int i = 0; i < urls.length; i++) {
        AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(10000);
        String[] allowedContentTypes = new String[] { "image/png", "image/jpeg" };
            client.get(urls[i], new BinaryHttpResponseHandler(allowedContentTypes) {
                @Override
                public void onSuccess(byte[] fileData) {
                    // Do something with the file
                    Log.d("BLAH", "IT HAPPENED!");
                    Bitmap imageBitmap = BitmapFactory.decodeByteArray(fileData, 0, fileData.length);
                    //if (gameDetailsFragment != null){
                    //gameDetailsFragment.setImageView(imageBitmap);
                    images.add(imageBitmap);
                    //}

                }
            });
        }

        gameOptionsFragment = new GameOptionsFragment(options);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.gameDetailsContainer, new GameDetailsFragment(event, images))
                    .add(R.id.gameOptionsContainer, gameOptionsFragment)
                    .commit();
        }
    }



    public void onDetailViewUpdate(int position) {
        /*if (event.size() >= 6) { //|| event.get(0) == "FinalText") { //<- commented out for testing purposes
                event.remove(0);
        }*/
        // For testing purposes only - event list will be filled from JSON parsing
        if (!welcome){

            event.add(game.getNextEvent(position).getText());
            if (game.getCurrentEvent().getId().equals("log") || game.getCurrentEvent().getId().equals("path") || game.getCurrentEvent().getId().equals("monster") || game.getCurrentEvent().getId().equals("friendly"))
                change = game.getCurrentEvent().getId();
            if (game.getCurrentEvent().getId().equals("diedbecausejerk"))
                gameDetailsFragment.setProgress(109,0);
        }
        if (event.get(0).equals("Welcome to FinalText!\n\nYou are now embarking on a Journey through Text!\n\nTo begin the game, press Play! below")) {
            event.remove(0);
            event.add(game.startEvent().getText());
            change = "path";

            welcome = false;
            //event.add("Random Generated Event will Go Here");
        }
        options.clear();


        //for (int i = 0; i < game.getEventOptions().size(); i++)
        ArrayList<Option> optionArray = game.getEventOptions();
        System.out.println(optionArray.size() + " The Option Size");
        for (int i = 0; i<optionArray.size(); i++){
            options.add(optionArray.get(i).getText());
            System.out.println("Added option");
        }

        if (event.size() >= 50)
            event.remove(event.get(0));

        gameDetailsFragment = new GameDetailsFragment(event, images);
        gameOptionsFragment = new GameOptionsFragment(options);
        getFragmentManager().beginTransaction()
                .replace(R.id.gameDetailsContainer, gameDetailsFragment)
                .replace(R.id.gameOptionsContainer, gameOptionsFragment)
                .addToBackStack("DetailBack")
                .addToBackStack("OptionBack")
                .commit();
        if (isFighter) {
        AsyncHttpClient client = new AsyncHttpClient();
        String[] allowedContentTypes = new String[] { "image/png", "image/jpeg" };
        //for (int i = 0; i < urls.length; i++) {
        client.get("http://images.wikia.com/streetfighter/images/archive/e/e5/20110413090131!Super_Street_Fighter_II_X_Art_Ryu_1.jpg", new BinaryHttpResponseHandler(allowedContentTypes) {
            @Override
            public void onSuccess(byte[] fileData) {
                // Do something with the file
                Log.d("BLAH", "IT HAPPENED!");
                Bitmap imageBitmap = BitmapFactory.decodeByteArray(fileData, 0, fileData.length);
                if (gameDetailsFragment != null){
                    gameDetailsFragment.setImageView(imageBitmap);
                    images.add(imageBitmap);
                }

            }
        });
        }
        if (change.equals("monster")) {
            changed = true;
            AsyncHttpClient client = new AsyncHttpClient();
            String[] allowedContentTypes = new String[] { "image/png", "image/jpeg" };
            //for (int i = 0; i < urls.length; i++) {
            client.get(urls[3], new BinaryHttpResponseHandler(allowedContentTypes) {
                @Override
                public void onSuccess(byte[] fileData) {
                    // Do something with the file
                    Log.d("BLAH", "IT HAPPENED!");
                    Bitmap imageBitmap = BitmapFactory.decodeByteArray(fileData, 0, fileData.length);
                    if (gameDetailsFragment != null){
                        gameDetailsFragment.setEventImageView(imageBitmap);
                        images.add(imageBitmap);
                    }

                }
            });
        }
        else if (change.equals("path")) {
            changed = true;
            AsyncHttpClient client = new AsyncHttpClient();
            String[] allowedContentTypes = new String[] { "image/png", "image/jpeg" };
            //for (int i = 0; i < urls.length; i++) {
            client.get(urls[2], new BinaryHttpResponseHandler(allowedContentTypes) {
                @Override
                public void onSuccess(byte[] fileData) {
                    // Do something with the file
                    Log.d("BLAH", "IT HAPPENED!");
                    Bitmap imageBitmap = BitmapFactory.decodeByteArray(fileData, 0, fileData.length);
                    if (gameDetailsFragment != null){
                        gameDetailsFragment.setEventImageView(imageBitmap);
                        images.add(imageBitmap);
                    }

                }
            });
        }
        else if (change.equals("log")) {
            changed = true;
            AsyncHttpClient client = new AsyncHttpClient();
            String[] allowedContentTypes = new String[] { "image/png", "image/jpeg" };
            //for (int i = 0; i < urls.length; i++) {
            client.get(urls[4], new BinaryHttpResponseHandler(allowedContentTypes) {
                @Override
                public void onSuccess(byte[] fileData) {
                    // Do something with the file
                    Log.d("BLAH", "IT HAPPENED!");
                    Bitmap imageBitmap = BitmapFactory.decodeByteArray(fileData, 0, fileData.length);
                    if (gameDetailsFragment != null){
                        gameDetailsFragment.setEventImageView(imageBitmap);
                        images.add(imageBitmap);
                    }

                }
            });
        }
        else if (change.equals("friendly")) {
            changed = true;
            AsyncHttpClient client = new AsyncHttpClient();
            String[] allowedContentTypes = new String[] { "image/png", "image/jpeg" };
            //for (int i = 0; i < urls.length; i++) {
            client.get(urls[5], new BinaryHttpResponseHandler(allowedContentTypes) {
                @Override
                public void onSuccess(byte[] fileData) {
                    // Do something with the file
                    Log.d("BLAH", "IT HAPPENED!");
                    Bitmap imageBitmap = BitmapFactory.decodeByteArray(fileData, 0, fileData.length);
                    if (gameDetailsFragment != null){
                        gameDetailsFragment.setEventImageView(imageBitmap);
                        images.add(imageBitmap);
                    }

                }
            });
        }
        System.out.println("Size of Image ArrayList " + images.size());

        if (game.getEventOptions().get(0).getText().equals("Reset")) {
            //game = new FinalTextGame(json);
            options.clear();
            //options.add("Replay!");
            event.clear();
            event.add(game.startEvent().getText());
            optionArray = game.getEventOptions();
            for (int i = 0; i<optionArray.size(); i++){
                options.add(optionArray.get(i).getText());
            }
            change = "path";
        }
        //gameDetailsFragment.addEvent(this, eText, eImage);
/*
                if (gameDetailsFragment != null){
                    gameDetailsFragment.setImageView(eImage);
                    //images.add(imageBitmap);
                }*/

    }





}
