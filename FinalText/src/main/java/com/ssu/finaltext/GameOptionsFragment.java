package com.ssu.finaltext;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

/**
 * Created by sean on 12/3/13.
 */
public class GameOptionsFragment extends Fragment implements AdapterView.OnItemClickListener{
    List<String> items;
    ListView listView;
    ArrayAdapter<String> adapter;
    Context context = getActivity();
    OnDetailView mListener;

    public GameOptionsFragment(List<String> items) {
        this.items = items;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = activity;
        if (activity instanceof OnDetailView){
            mListener = (OnDetailView) activity;
        }else{
            throw new ClassCastException (activity.toString() + " is lame!");
        }
    }

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.options_layout, container, false);



        // In fragments, we don't have access to everything in the activity, only what is
        // created or passed to the fragment.
        listView = (ListView)view.findViewById(R.id.gameOptionsView);
        adapter = new ArrayAdapter<String>(context, R.layout.custom_textview, items);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        return view;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mListener.onDetailViewUpdate(position);

    }

    public interface OnDetailView{
        public void onDetailViewUpdate(int position);
    }
}