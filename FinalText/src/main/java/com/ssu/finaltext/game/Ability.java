package com.ssu.finaltext.game;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by sean on 11/25/13.
 */
public class Ability extends FinalText {

    // The effects to apply to target character
    private ArrayList<eType> effectsAdded;
    private boolean selfTarget;

    // Make "damageMin" the higher number if this ability will heal target
    protected int damageMin;
    protected int damageMax;

    Ability(){
        this.id = "0";
        this.name = "";
        this.ftType = Type.ABILITY;
        this.selfTarget = false;

    }

    private boolean isSelfTarget(){
        return selfTarget;
    }

    // how much damage to deal to the affected character
    // negative values will heal
    public int getDamageValue(){

        Random rand = new Random();
        rand.setSeed(System.currentTimeMillis());

        // Returns a value between the min and max damage (completely random)
        int damageValue = rand.nextInt(damageMax-damageMin) + damageMin;

        return damageValue;

    }

    // The effects to add to the affected character
    public ArrayList<eType> getEType(){

        return effectsAdded;
    }

}
