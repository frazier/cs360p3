package com.ssu.finaltext.game;

import java.util.ArrayList;

/**
 * Created by sean on 11/19/13.
 */

// Suggestions:




public class Character extends FinalText {
    enum CharacterType{NPC, PLAYER}
    CharacterType cType;
    protected int hitPoints;
    protected int hitPointsMax;
    /*
    protected float armorValue;
    protected int level;
    protected ArrayList<Effect> activeEffects;
    */
    protected ArrayList<Ability> abilities;


    public static class Effect {

        /* Example: Each turn, character or monster takes poison damage */
        // Poison: Deals damage each turn
        // Mortal Wound: Reduces maximum health
        // Delusion: chance to miss with attacks
        // Stun: chance to skip your turn


        // the "factor" of the effect (yes this game will be brutal
        public int power;
        public eType effectType;

        public eType getType(){
            return effectType;
        }

    };

    // Creates a new NPC character and initializes all attributes to default
    public Character(){
        // this.level=0;
        this.hitPoints = this.hitPointsMax=1;
        // this.activeEffects = new ArrayList<Effect>();
        this.abilities = new ArrayList<Ability>();

    }

    // Creates a new character of a specified type with specified attributes
    Character( CharacterType cType, int level, int hitPointsMax ){
        //this.level=level;
        this.hitPoints=this.hitPointsMax=hitPointsMax;
        this.cType=cType;
        //this.activeEffects=new ArrayList<Effect>();
        this.abilities = new ArrayList<Ability>();
    }

    public CharacterType getCType(){
        return cType;
    }

    public int[] getHitPoints(){
        int [] hp;
        hp = new int[2];
        hp[0] = this.hitPoints;
        hp[1] = this.hitPointsMax;
        return hp;

    }

    public void setHitPoints(int number){
        this.hitPoints=this.hitPointsMax = number;
    }


    // Modify is the number of hitpoints to add or subtract from the hitPoints or hitPointsMax variables
    // modify[0] are for changing the current hitpoints of the character
    // modify[1] are for changing the maximum hitpoints of the character
    // hitpoints should never be more than hitPointsMax
    public void modHitPoints(int modify[]){
        this.hitPointsMax += modify[1];
        if ((this.hitPoints + modify[0]) <= this.hitPointsMax){
            this.hitPoints += modify[0];
        }

    }

    public void takeDamage(int damageValue){
        // Calc damage dealt to this character based on armor values and the damage value
        if (this.hitPoints - damageValue > 0)
            this.hitPoints = this.hitPoints-damageValue;
        else
            // Handle character death
            System.out.println("Character is dead");
    }

    public Ability useAbility(int abilityNumber){

        return abilities.get(abilityNumber);
    }

    public void recieveAbilityHit(FinalText ability){
        if (ability.getGType()!= Type.ABILITY){
            return;
        }

        System.out.println("Handle hit by ability");

    }
/*
    // Removes a specific effect from the character
    public void removeOneEffect(FinalText.eType effectType){
        if (this.activeEffects.size() == 0)
            return;


        updateCharacter();

    }
    */
    // Removes one random effect from the character
    /*
    public void removeOneEffect(){
        if (this.activeEffects.size() != 0){
            Random generator = new Random(System.currentTimeMillis());
            int randomEffect = generator.nextInt(this.activeEffects.size());
            this.activeEffects.remove(randomEffect);
        }
        updateCharacter();
    }
*/
    /*
    public void removeAllEffects(){
        if (this.activeEffects.size() != 0){
            this.activeEffects.clear();
        }
        updateCharacter();
    }
    */
    public void updateCharacter(){
        // Set attack and ability power based on character stats.
    }

    public void fromString(String input){
        if (input == "NPC")
            this.cType = cType.NPC;
        else
            this.cType = cType.PLAYER;
        /* Do the other stuff to read in a new game object */
    }

}
