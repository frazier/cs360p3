package com.ssu.finaltext.game;

import java.util.ArrayList;

/* Simple Class to hold an event for the gameDetails
 * Includes successiveEvents - a way to keep track of which events are permitted immediately after
 * this event!
 * Created by sean on 11/25/13.
 */

public class Event extends FinalText {

    enum EventCondition{LOSE, CONTINUE, WIN};

    EventCondition eventCondition;

    // The text to output when an event occurs
    String eventText;

    // If the event begins combat:
    boolean startsBattle;

    // IDs of all possible events that can occur after this event (in case we want to limit)
    ArrayList<String> options;

    public Event(){
        this.eventText = "Unset Event!";
        this.options = new ArrayList<String>();
        this.startsBattle = false;
        this.eventCondition = EventCondition.CONTINUE;
    }

    public void setEventCondition (int condition){
        if (condition == 0){
            this.eventCondition = EventCondition.LOSE;
        }
        else if (condition == 1){
            this.eventCondition = EventCondition.CONTINUE;
        }
        else if (condition == 2){
            this.eventCondition = EventCondition.CONTINUE;
        }
    }

    public EventCondition getEventCondition(){
        return eventCondition;
    }

    // Set the event's text
    public void setText(String text){
        this.eventText = text;
    }

    // get the event's text
    public String getText(){
        return this.eventText;
    }

    public void setStartsBattle (boolean b){
        startsBattle = b;
    }
    public boolean getStartsBattle(){
        return startsBattle;
    }

    public void addOption(String optionIDtoAdd){
        this.options.add(optionIDtoAdd);
    }

    public void setEventOptions(String s) {
        String optionsList[] = s.split("\\,");
        for (int i=0; i<optionsList.length; i++){
            options.add(optionsList[i]);
            System.out.println(optionsList[i] + "Added Event Option");
        }
    }

    public String getOptionSelected(int position){
        String optionID = options.get(position);
        return optionID;

    }

    public ArrayList<String> getOptions (){
        return options;
    }

    // read options in from string - should be formatted as follows:
    // s = "Event text here | <comma separated list of ids to proceeding events>"
    // We can definitely change this around any way we need to
    public void fromString(String s){

        // Check format: make sure string is properly formatted, if so: do magic
        if (s.contains("|")){
            String parts[] = s.split("\\|");
            id = parts[0];
            if (parts[1] == "true") startsBattle=true;
            eventText = parts[2];
            String ids[] = parts[3].split("\\,");
            // Adds all option IDs to the arraylist
            for (int i=0; i<ids.length; i++){
                options.add(ids[i].trim());
            }
        }
        // Check format: otherwise, whine about it...
        else{
            System.err.println("Error: Improper formatting on string to event - ignoring: \n\t" + s);
        }
    }

}