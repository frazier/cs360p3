package com.ssu.finaltext.game;
/**
 * Created by sean on 11/19/13.
 */

public class FinalText {
    public enum Type {GAME, CHARACTER, NPC, ITEM, ABILITY, EVENT, OPTION};
    public enum eType{POISON, MORTALWOUND, DELUSION, STUN};
    // Game id's should start with 'G'
    protected String id;
    protected Type ftType;
    protected String name;



    public FinalText(){

    }

    public FinalText(String id, Type entityType){
        this.id = id;
        this.ftType = entityType;
    }
    public FinalText(String id, Type entityType, String name){
        this.id = id;
        this.ftType = entityType;
        this.name = name;
    }

    public void setID(String newID){
        this.id = newID;
        System.out.println(this.id + " The ID");
    }

    public String getId() {
        return id;
    }
    public boolean hasID(String id){
        System.out.println(this.id + id + "Checking ID");
        if (this.id.equals(id)) return true;
        else return false;
    }

    public void setName(String newName){
        this.name = newName;
    }

    public String getName(){
        return name;
    }

    public String getText() {return "";}

    public void setGType(Type type){this.ftType = type;}
    public Type getGType(){
        return ftType;
    }

}
