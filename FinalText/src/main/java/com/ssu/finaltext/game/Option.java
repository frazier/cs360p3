package com.ssu.finaltext.game;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by student on 12/11/13.
 */
public class Option extends FinalText {
    private ArrayList<String> possibleEvents;
    private String optionText;
    private Random generator;

    public Option(){
        generator = new Random(System.currentTimeMillis());
        possibleEvents = new ArrayList<String>();
    }


    public void setText(String s){optionText = s;}
    public String getText(){return optionText;}

    public void setOptionText(String optionText){
        this.optionText = optionText;
    }

    // Returns the id of an event that is possible from this option
    public String getRandomEventID(){
        int eventIndex = generator.nextInt(possibleEvents.size());
        return possibleEvents.get(eventIndex);
    }

    // Takes a string with comma delimiters and reads the individual ids into the possibleEvents list
    public void setPossibleEvents(String s) {
        String eventsList[] = s.split("\\,");
        for (int i=0; i<eventsList.length; i++){
            possibleEvents.add(eventsList[i]);
        }
    }

    public void fromString(String input){
        // Check format: make sure string is properly formatted, if so: do magic
        if (input.contains("|")){
            String parts[] = input.split("\\|");
            optionText = parts[0];
            String ids[] = parts[1].split("\\,");
            // Adds all event IDs to the arraylist
            for (int i=0; i<ids.length; i++){
                possibleEvents.add(ids[i].trim());
            }
        }
        // Check format: otherwise, whine about it...
        else{
            System.err.println("Error: Improper formatting on string to event - ignoring: \n\t" + input);
        }
    }

}
